### Description
The Arduino is a small microcontroller with a USB plug and a some connection sockets that can be connected to external components, such as motors, relays, sensors, diodes, speakers, microphones, etc. Arduinos can be powered either through the USB connection from the computer or from a 9V battery. They can be controlled from the computer or programmed by the computer and then disconnected and allowed to work independently.

Various programs are available for download in this repo. You can either clone it or download it as a single Zip. The whole download is less than a Gigabyte, so it makes sense to download the software for all of the projects, even if you only intend to use a few.

Whatever your platform, the Arduino software looks for your programs in ~/Documents/Arduino which the IDE will create the first time it is run. So clone the repo, or alternatively, place the contents of the Zip file into that folder. Note that each program comes in its own folder and they are numbered by project.

### Hardware Required

* Arduino Uno
* Multipurpose Shield
* DHT temperature and humidity sensor
* SONAR sensor
* Luminescence sensor
* resistors
* Jumper wires
* USB Type-A

### Software Required

* Arduino IDE
